﻿using Android.App;
using Android.Widget;
using Android.OS;
using Android.Content.PM;
using System;

namespace thinkforce_app.Droid
{
	[Activity (
		ScreenOrientation = ScreenOrientation.Portrait, 
		Label = "Thinkforce", 
		MainLauncher = true, 
		Icon = "@mipmap/icon", 
		Theme = "@style/LaunchTheme")]
	
	public class MainActivity : Activity
	{
		public MainView ContentView { get; set; }

		protected async override void OnCreate (Bundle savedInstanceState)
		{
			Device.Measure (this);

			base.OnCreate (savedInstanceState);

			ContentView = new MainView (this);

			SetContentView (ContentView);

			ContentView.LayoutSubviews ();

//			await Networking.GetLampStates ();

			ContentView.Bar.Initialize ();

			Client.Instance.Subscribe (OnMessageReceived);
		}

		void OnMessageReceived (SendData data)
		{
			Console.WriteLine ("OnMessageReceived: " + data.Message);
		}
	}
}


