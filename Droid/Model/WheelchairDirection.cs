﻿using System;

namespace thinkforce_app.Droid
{
	public enum MovementDirection
	{
		None,
		Forward,
		Reverse,
		Stop,
		Left,
		Right
	}

	public enum LampColor
	{
		None = 0,
		Yellow = 10000,
		Green = 20000,
		Blue = 40000,
		Red = 60000
	}
}

