﻿using System;

namespace thinkforce_app.Droid
{
	public class LampState
	{
		public string Name { get; set; }

		public bool IsOn { get; set; }

		public int Number  { 
			get {

				string number = Name.Substring (Name.Length - 2);

				int result = -1;
				int.TryParse (number, out result);

				return result;
			} 
		}

		public LampState ()
		{
			
		}
	}
}

