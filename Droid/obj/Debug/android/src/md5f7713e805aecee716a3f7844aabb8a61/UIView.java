package md5f7713e805aecee716a3f7844aabb8a61;


public class UIView
	extends android.widget.RelativeLayout
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_getAlpha:()F:__export__\n" +
			"n_setAlpha:(F)V:__export__\n" +
			"n_getTopMargin:()F:__export__\n" +
			"n_setTopMargin:(F)V:__export__\n" +
			"n_getLeftMargin:()F:__export__\n" +
			"n_setLeftMargin:(F)V:__export__\n" +
			"n_getNewWidth:()I:__export__\n" +
			"n_setNewWidth:(I)V:__export__\n" +
			"n_getNewHeight:()I:__export__\n" +
			"n_setNewHeight:(I)V:__export__\n" +
			"n_addView:(Landroid/view/View;)V:GetAddView_Landroid_view_View_Handler\n" +
			"";
		mono.android.Runtime.register ("thinkforce_app.Droid.UIView, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", UIView.class, __md_methods);
	}


	public UIView (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == UIView.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UIView, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public UIView (android.content.Context p0, android.util.AttributeSet p1) throws java.lang.Throwable
	{
		super (p0, p1);
		if (getClass () == UIView.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UIView, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public UIView (android.content.Context p0, android.util.AttributeSet p1, int p2) throws java.lang.Throwable
	{
		super (p0, p1, p2);
		if (getClass () == UIView.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UIView, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public UIView (android.content.Context p0, android.util.AttributeSet p1, int p2, int p3) throws java.lang.Throwable
	{
		super (p0, p1, p2, p3);
		if (getClass () == UIView.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UIView, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2, p3 });
	}


	public float getAlpha ()
	{
		return n_getAlpha ();
	}

	private native float n_getAlpha ();


	public void setAlpha (float p0)
	{
		n_setAlpha (p0);
	}

	private native void n_setAlpha (float p0);


	public float getTopMargin ()
	{
		return n_getTopMargin ();
	}

	private native float n_getTopMargin ();


	public void setTopMargin (float p0)
	{
		n_setTopMargin (p0);
	}

	private native void n_setTopMargin (float p0);


	public float getLeftMargin ()
	{
		return n_getLeftMargin ();
	}

	private native float n_getLeftMargin ();


	public void setLeftMargin (float p0)
	{
		n_setLeftMargin (p0);
	}

	private native void n_setLeftMargin (float p0);


	public int getNewWidth ()
	{
		return n_getNewWidth ();
	}

	private native int n_getNewWidth ();


	public void setNewWidth (int p0)
	{
		n_setNewWidth (p0);
	}

	private native void n_setNewWidth (int p0);


	public int getNewHeight ()
	{
		return n_getNewHeight ();
	}

	private native int n_getNewHeight ();


	public void setNewHeight (int p0)
	{
		n_setNewHeight (p0);
	}

	private native void n_setNewHeight (int p0);


	public void addView (android.view.View p0)
	{
		n_addView (p0);
	}

	private native void n_addView (android.view.View p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
