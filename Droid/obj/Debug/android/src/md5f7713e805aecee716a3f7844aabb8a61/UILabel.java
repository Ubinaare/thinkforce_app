package md5f7713e805aecee716a3f7844aabb8a61;


public class UILabel
	extends android.widget.TextView
	implements
		mono.android.IGCUserPeer
{
	static final String __md_methods;
	static {
		__md_methods = 
			"n_getAlpha:()F:__export__\n" +
			"n_setAlpha:(F)V:__export__\n" +
			"n_getLeftMargin:()F:__export__\n" +
			"n_setLeftMargin:(I)V:__export__\n" +
			"";
		mono.android.Runtime.register ("thinkforce_app.Droid.UILabel, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", UILabel.class, __md_methods);
	}


	public UILabel (android.content.Context p0) throws java.lang.Throwable
	{
		super (p0);
		if (getClass () == UILabel.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UILabel, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0 });
	}


	public UILabel (android.content.Context p0, android.util.AttributeSet p1) throws java.lang.Throwable
	{
		super (p0, p1);
		if (getClass () == UILabel.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UILabel, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065", this, new java.lang.Object[] { p0, p1 });
	}


	public UILabel (android.content.Context p0, android.util.AttributeSet p1, int p2) throws java.lang.Throwable
	{
		super (p0, p1, p2);
		if (getClass () == UILabel.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UILabel, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2 });
	}


	public UILabel (android.content.Context p0, android.util.AttributeSet p1, int p2, int p3) throws java.lang.Throwable
	{
		super (p0, p1, p2, p3);
		if (getClass () == UILabel.class)
			mono.android.TypeManager.Activate ("thinkforce_app.Droid.UILabel, thinkforce_app.Droid, Version=1.0.0.0, Culture=neutral, PublicKeyToken=null", "Android.Content.Context, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:Android.Util.IAttributeSet, Mono.Android, Version=0.0.0.0, Culture=neutral, PublicKeyToken=84e04ff9cfb79065:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e:System.Int32, mscorlib, Version=2.0.5.0, Culture=neutral, PublicKeyToken=7cec85d7bea7798e", this, new java.lang.Object[] { p0, p1, p2, p3 });
	}


	public float getAlpha ()
	{
		return n_getAlpha ();
	}

	private native float n_getAlpha ();


	public void setAlpha (float p0)
	{
		n_setAlpha (p0);
	}

	private native void n_setAlpha (float p0);


	public float getLeftMargin ()
	{
		return n_getLeftMargin ();
	}

	private native float n_getLeftMargin ();


	public void setLeftMargin (int p0)
	{
		n_setLeftMargin (p0);
	}

	private native void n_setLeftMargin (int p0);

	java.util.ArrayList refList;
	public void monodroidAddReference (java.lang.Object obj)
	{
		if (refList == null)
			refList = new java.util.ArrayList ();
		refList.add (obj);
	}

	public void monodroidClearReferences ()
	{
		if (refList != null)
			refList.clear ();
	}
}
