﻿using System;
using Android.App;
using System.Collections.Generic;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class TabBar : UIView
	{
		public List<Tab> Tabs;
		public UIView separator1, separator2;

		Activity context;

		public TabBar (Activity context) : base (context)
		{
			this.context = context;

			BackgroundColor = Header.COLOR;

			Tabs = new List<Tab> {
				new Tab (context) { ImageResource = Resource.Drawable.image_wheelchair },
				new Tab (context) { ImageResource = Resource.Drawable.image_hue },
				new Tab (context) { ImageResource = Resource.Drawable.image_bb8 }
			};

			foreach (Tab tab in Tabs) {
				AddView (tab);
				tab.TabSelected += OnTabSelected;
			}

			separator1 = new UIView (context) { BackgroundColor = Color.White };
			separator2 = new UIView (context) { BackgroundColor = Color.White };

			AddViews (separator1, separator2);
		}

		int SEPARATORWIDTH = 1;

		public override void LayoutSubviews ()
		{
			int padding = 20;
			int smallPadding = 5;

			int x = smallPadding;
			int y = smallPadding;
			int w = (Frame.W - 2 * SEPARATORWIDTH - 6 * smallPadding) / 3;
			int h = Frame.H - 2 * smallPadding;

			foreach (Tab tab in Tabs) {
				tab.Frame = new Frame (x, y, w, h);

				x += w + smallPadding;

				if (separator1.Frame.IsEmpty) {
					separator1.Frame = new Frame (x, padding, SEPARATORWIDTH, h - 2 * padding);
				} else if (separator2.Frame.IsEmpty) {
					separator2.Frame = new Frame (x, padding, SEPARATORWIDTH, h - 2 * padding);
				}	

				x += SEPARATORWIDTH + smallPadding;

			}
		}

		public void Initialize ()
		{
			OnTabSelected (Tabs [0], EventArgs.Empty);	
		}

		public Tab currentActive;

		public void OnTabSelected (object sender, EventArgs e)
		{
			if (currentActive != null) {
				currentActive.Normalize ();
			}

			currentActive = (Tab)sender;

			currentActive.Highlight ();

			(context as MainActivity).ContentView.OnTabSelected (Tabs.IndexOf (currentActive));
		}
	}

	public class Tab : UIView
	{
		public EventHandler<EventArgs> TabSelected;

		UIImageView imageView;

		public int ImageResource {
			get {
				return imageView.ImageResource;
			} set {
				imageView.ImageResource = value;
			}
		}

		public Tab (Activity context) : base (context)
		{
			BackgroundColor = Header.COLOR;

			imageView = new UIImageView (context);

			AddView (imageView);

			Click += delegate {
				TabSelected(this, EventArgs.Empty);
			};
		}

		public override void LayoutSubviews ()
		{
			int h = Frame.H;
			int w = h;
			int x = Frame.W / 2 - w / 2;
			int y = 0;

			imageView.Frame = new Frame (x, y, w, h);
		}

		public void Normalize ()
		{
			BackgroundColor = Header.COLOR;
		}

		public void Highlight ()
		{
			BackgroundColor = Header.ACTIVECOLOR;
		}
	}
}

