﻿using System;
using Android.Graphics;
using Android.App;

namespace thinkforce_app.Droid
{
	public class ColorButton : UIView
	{
		public Color Color {
			get {
				return BackgroundColor;
			} set {
				BackgroundColor = value;
			}
		}

		public ColorButton (Activity context) : base (context)
		{

		}

		public override void LayoutSubviews ()
		{
			Round ();
		}
	}
}

