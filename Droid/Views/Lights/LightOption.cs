﻿using System;
using Android.App;
using Android.Graphics;
using Android.Widget;

namespace thinkforce_app.Droid
{
	public class LightOption : UIView
	{
		UILabel title;
		public Switch Switch;
		public ColorContainer Colors;

		public string Text {
			get {
				return title.Text;
			} set {
				title.Text = value;
			}
		}

		public int LightIndex { get; set; }

		public LightOption (Activity context, int lightIndex) : base (context)
		{
			this.LightIndex = lightIndex;

			BackgroundColor = Color.Rgb (230, 230, 230);

			title = new UILabel (context);
			title.BackgroundColor = Color.Transparent;
			title.Gravity = Android.Views.GravityFlags.Center;
			title.Font = Font.Get (FontStyle.SerifLight, 30);
			title.TextColor = Color.Black;

			Switch = new Switch (context);
			Switch.SetBackgroundColor (Color.Transparent);
			Colors = new ColorContainer (context, lightIndex);

			AddViews (title, Switch, Colors);
		}

		public override void LayoutSubviews ()
		{
			int x = 0;
			int y = 0;
			int w = Frame.W;
			int h = Frame.H / 5;

			title.Frame = new Frame (x, y, w, h);

			y += h;

			int switchX = Frame.W / 2 - Switch.SwitchMinWidth / 2;
			Switch.LayoutParameters = LayoutUtils.GetRelative(switchX, y, Switch.SwitchMinWidth, h);

			y += h;
			h = Frame.H - (2 * h);

			Colors.Frame = new Frame (x, y, w, h);
		}
	}
}

