﻿using System;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class ColorContainer : UIView
	{
		public EventHandler<LightEventArgs> ColorClicked;

		public ColorButton Green, Blue, Red, Yellow;

		public int LightIndex { get; set; }

		public ColorContainer (Activity context, int lightIndex) : base (context)
		{
			LightIndex = lightIndex;

			Green = new ColorButton (context) { Color = Color.Green };

			Blue = new ColorButton (context) { Color = Color.Blue };

			Red = new ColorButton (context) { Color = Color.Red };

			Yellow = new ColorButton (context) { Color = Color.Yellow };

			AddViews (Green, Blue, Red, Yellow);

			Green.Click += OnColorClick;
			Blue.Click += OnColorClick;
			Red.Click += OnColorClick;
			Yellow.Click += OnColorClick;
		}

		public override void LayoutSubviews ()
		{
			int padding = 5;

			int w = (Frame.H - 3 * padding) / 2;
			int h = w;

			int x = Frame.W / 2 - ((2 * w + 3 * padding) / 2);
			int y = 5;

			Green.Frame = new Frame (x, y, w, h);

			x += w + padding;

			Blue.Frame = new Frame (x, y, w, h);

			y += h + padding;
			x = Frame.W / 2 - ((2 * w + 3 * padding) / 2);

			Red.Frame = new Frame (x, y, w, h);

			x += w + padding;

			Yellow.Frame = new Frame (x, y, w, h);
		}

		public void OnColorClick (object sender, EventArgs e)
		{
			if (ColorClicked != null) {
				ColorClicked (sender, new LightEventArgs { LightNumber = LightIndex });
			}	
		}

	}
}

