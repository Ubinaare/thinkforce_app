﻿using System;
using Android.App;
using Android.Graphics;
using Android.Widget;

namespace thinkforce_app.Droid
{
	public class LightsView : UIView
	{
		LightOption option1, option2, option3, option4;

		public LightsView (Activity context) : base (context)
		{
			option1 = new LightOption (context, 0) { Text = "LIGHT 1" };
			option2 = new LightOption (context, 1) { Text = "LIGHT 2" };
			option3 = new LightOption (context, 2) { Text = "LIGHT 3" };
			option4 = new LightOption (context, 3) { Text = "LIGHT 4" };

			AddViews (option1, option2, option3, option4);

			option1.Colors.ColorClicked += OnColorClicked;
			option2.Colors.ColorClicked += OnColorClicked;
			option3.Colors.ColorClicked += OnColorClicked;
			option4.Colors.ColorClicked += OnColorClicked;

			option1.Switch.Click += OnSwitchClicked;
			option2.Switch.Click += OnSwitchClicked;
			option3.Switch.Click += OnSwitchClicked;
			option4.Switch.Click += OnSwitchClicked;
		}

		public override void LayoutSubviews ()
		{
			int innerPadding = 10;

			int x = innerPadding;
			int y = 0;
			int w = (Frame.W - 3 * innerPadding) / 2;
			int h = 350;

			option1.Frame = new Frame (x, y, w, h);

			x += w + innerPadding;

			option2.Frame = new Frame (x, y, w, h);

			x = innerPadding;
			y += h + innerPadding;

			option3.Frame = new Frame (x, y, w, h);

			x += w + innerPadding;

			option4.Frame = new Frame (x, y, w, h);
		}

		public async void OnColorClicked (object sender, LightEventArgs e)
		{
			ColorButton button = (ColorButton)sender;

			int index = e.LightNumber;
			LampColor color;

			if (button.BackgroundColor == Color.Green) {
				color = LampColor.Green;
			} else if (button.BackgroundColor == Color.Blue) {
				color = LampColor.Blue;
			} else if (button.BackgroundColor == Color.Yellow) {
				color = LampColor.Yellow;
			} else if (button.BackgroundColor == Color.Red) {
				color = LampColor.Red;
			} else {
				color = LampColor.None;
			}

			await Networking.TurnColorOfLampOfIndex (index, color);

			Console.WriteLine ("Clicked " + button.BackgroundColor + " of lamp " + index);
		}

		public async void OnSwitchClicked (object sender, EventArgs e)
		{
			Switch item = (Switch)sender;
			LightOption parent = (LightOption)item.Parent;
			int index = parent.LightIndex;

			await Networking.TurnLightOfIndex (index, item.Checked);
			Console.WriteLine ("Clicked switch of lamp " + index);
		}

	}
}

