﻿using System;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class Separator : UIView
	{
		UIView line;
		UILabel text;

		public string Text {
			get {
				return text.Text;
			} set {
				text.Text = value;
			}
		}

		public Separator (Activity context) : base (context)
		{
			BackgroundColor = Color.Transparent;

			line = new UIView (context);
			line.BackgroundColor = Color.LightGray;

			text = new UILabel (context);
			text.TextColor = Color.Gray;
			text.Font = Font.Get (FontStyle.SerifLight, 30);
			text.Gravity = Android.Views.GravityFlags.CenterVertical;
			text.BackgroundColor = Color.Transparent;

			AddViews (line, text);
		}

		const int LINEHEIGHT = 1;

		public override void LayoutSubviews ()
		{
			int padding = 10;
			int largePadding = 20;

			int x = largePadding;
			int y = padding;
			int w = Frame.W - 2 * largePadding;
			int h = LINEHEIGHT;

			line.Frame = new Frame (x, y, w, h);

			y += h + padding;
			h = Frame.H - (3 * padding + LINEHEIGHT);

			text.Frame = new Frame (x, y, w, h);
		}
	}
}

