﻿using System;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class MainView : UIView
	{
		public Header Header;
		public TabBar Bar;

		public LightsView Lights;
		public WheelchairView Wheelchair;
		public BB8View BB8;

		Separator subHeader;

		public MainView (Activity context) : base(context)
		{
			BackgroundColor = Color.Rgb (245, 245, 245);

			Header = new Header (context);

			Bar = new TabBar (context);

			subHeader = new Separator (context) { Text = "SUBHEADER" };

			Lights = new LightsView (context);

			Wheelchair = new WheelchairView (context);

			BB8 = new BB8View (context);

			AddViews (
				Header,
				Bar,
				subHeader, 
				Lights, 
				Wheelchair, 
				BB8
			);
		}

		const int SEPARATORHEIGHT = 100;
		const int BARHEIGHT = 150;

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();

			int x = 0;
			int y = 0;
			int w = Device.ScreenWidth;
			int h = 100;

			Header.Frame = new Frame (x, y, w, h);

			y += h;

			h = BARHEIGHT;

			Bar.Frame = new Frame (x, y, w, h);

			y += h;

			subHeader.Frame = new Frame (x, y, w, SEPARATORHEIGHT);

			y += SEPARATORHEIGHT;

			h = Device.ScreenHeight - y;

			Lights.Frame = new Frame (x, y, w, h);

			Wheelchair.Frame = new Frame (x, y, w, h);

			BB8.Frame = new Frame (x, y, w, h);
		}

		public void OnTabSelected (int index)
		{
			if (index == 0) {
				subHeader.Text = "WHEELCHAIR";
				Wheelchair.AnimateShow (delegate {
					
				});
				Lights.AnimateHide (delegate {
					
				});
				BB8.AnimateHide (delegate {
					
				});
			} else if (index == 1) {
				subHeader.Text = "LIGHTS";
				Lights.AnimateShow (delegate {

				});
				Wheelchair.AnimateHide (delegate {

				});
				BB8.AnimateHide (delegate {

				});
			} else if (index == 2) {
				subHeader.Text = "BB-8";
				BB8.AnimateShow (delegate {

				});
				Lights.AnimateHide (delegate {

				});
				Wheelchair.AnimateHide (delegate {

				});
			}
		}
	}
}

