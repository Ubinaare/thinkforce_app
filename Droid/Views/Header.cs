﻿using System;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class Header : UIView
	{
		public static Color COLOR = Color.Rgb (48, 141, 146);
		public static Color ACTIVECOLOR = Color.Rgb (36, 105, 109);

		UILabel label;
		UIImageView headset;

		public Header (Activity context) : base (context)
		{
			BackgroundColor = COLOR;

			label = new UILabel (context) { Font = Font.Get (FontStyle.SerifThin, 32) };
			label.Text = "THINKFORCE CONTROL PANEL";
			label.TextColor = Color.White;
			label.Gravity = Android.Views.GravityFlags.Center;
			label.BackgroundColor = Color.Transparent;

			headset = new UIImageView (context);
			headset.ImageResource = Resource.Drawable.emotiv_headset;
			headset.FitImageToFrame ();
			headset.BackgroundColor = Color.Transparent;

			AddViews (label, headset);

			label.Measure (0, 0);
		}

		public override void LayoutSubviews ()
		{
			int padding = 10;
			int imageSize = Frame.H - 2 * padding;
			int labelWidth = label.MeasuredWidth;

			int x = Frame.W - (labelWidth + imageSize + 4 * padding);
			int y = 0;
			int h = Frame.H;
			int w = labelWidth;

			label.Frame = new Frame (x, y, w, h);

			x += w + 2 * padding;

			y = padding;
			w = imageSize;
			h = imageSize;

			headset.Frame = new Frame (x, y, w, h);
			headset.Round ();
		}
	}
}

