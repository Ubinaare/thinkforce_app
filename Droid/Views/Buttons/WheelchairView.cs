﻿using System;
using Android.App;
using Android.Graphics;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public class WheelchairView : ButtonsView
	{
		public WheelchairView (Activity context) : base (context)
		{
			Color background = Color.Rgb (18, 45, 56);

			foreach (MovementButton button in Buttons) {
				button.BackgroundColor = background;
				button.Click += OnButtonClick;
			}
		}

		public void OnButtonClick (object sender, EventArgs e)
		{
			MovementButton button = (MovementButton)sender;
			Console.WriteLine ("OnButtonClick: " + button.Direction);
			button.Alpha = 1f;

			Networking.DriveChair (button.Direction);
		}

	}
}

