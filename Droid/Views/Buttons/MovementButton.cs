﻿using System;
using Android.Graphics;
using Android.App;

namespace thinkforce_app.Droid
{
	public class MovementButton : UIImageView
	{
		public MovementDirection Direction { get; set; }

		public MovementButton (MovementDirection direction, Activity context) : base (context)
		{
			this.Direction = direction;
			BackgroundColor = Color.LightGray;

			ImageResource = GetImageResource ();

			int padding = 30;
			SetPadding (padding, padding, padding, padding);
		}

		int GetImageResource ()
		{
			if (Direction == MovementDirection.Forward) {
				return Resource.Drawable.ic_arrow_upward_white_48dp;
			} else if (Direction == MovementDirection.Reverse) {
				return Resource.Drawable.ic_arrow_downward_white_48dp;
			} else if (Direction == MovementDirection.Stop) {
				return 0;
			} else if (Direction == MovementDirection.Left) {
				return Resource.Drawable.ic_arrow_back_white_48dp;
			} else if (Direction == MovementDirection.Right) {
				return Resource.Drawable.ic_arrow_forward_white_48dp;
			}

			return 0;
		}

	}
}

