﻿using System;
using Android.App;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public class ButtonsView : UIView
	{
		public new MovementButton Left, Right;
		public MovementButton Forward, Reverse, Stop;

		public List<MovementButton> Buttons {
			get {
				return new List<MovementButton> { Left, Right, Forward, Reverse, Stop };
			}
		}

		public ButtonsView (Activity context) : base (context)
		{
			Left = new MovementButton (MovementDirection.Left, context);

			Right = new MovementButton (MovementDirection.Right, context);

			Forward = new MovementButton(MovementDirection.Forward, context);

			Reverse = new MovementButton (MovementDirection.Reverse, context);

			Stop = new MovementButton (MovementDirection.Stop, context);

			AddViews (Left, Right, Forward, Reverse, Stop);
		}


		public override void LayoutSubviews ()
		{
			int innerPadding = 15;
			int outerPadding = 50;

			int w = (Frame.W - (2 * outerPadding + 3 * innerPadding)) / 3;

			if (w > 180) {
				w = 180;
				outerPadding = (Frame.W - (3 * w + 2 * innerPadding)) / 2;
			}

			int h = w;

			int leftX = outerPadding;
			int middleX = leftX + w + innerPadding;
			int rightX = middleX + w + innerPadding;

			int x = middleX;
			int y = 50;

			Forward.Frame = new Frame (x, y, w, h);

			y += h + innerPadding;

			Stop.Frame = new Frame (x, y, w, h);

			y += h + innerPadding;

			Reverse.Frame = new Frame (x, y, w, h);

			y = Stop.Frame.Y;
			x = leftX;

			Left.Frame = new Frame (x, y, w, h);

			x = rightX;

			Right.Frame = new Frame (x, y, w, h);
		}

	}
}

