﻿using System;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class BB8View : ButtonsView
	{
		public BB8View (Activity context) : base (context)
		{
			Color background = Color.Rgb (214, 109, 26);

			foreach (MovementButton button in Buttons) {
				button.BackgroundColor = background;
				button.Click += OnButtonClick;
			}
		}

		public override void LayoutSubviews ()
		{
			base.LayoutSubviews ();
		}

		public void OnButtonClick (object sender, EventArgs e)
		{
			MovementButton button = (MovementButton)sender;
			Networking.DriveBB (button.Direction);	
		}
	}
}

