﻿using System;
using Android.App;
using Android.Widget;
using Android.Graphics;
using Java.Interop;
using Android.Animation;
using Android.Util;
using Android.Graphics.Drawables;
using Android.Graphics.Drawables.Shapes;
using Android.OS;

namespace thinkforce_app.Droid
{
	public class UIView : RelativeLayout
	{
		public const int MATCHPARENT = -1;

		public const string LEFT_MARGIN = "LeftMargin";
		public const string TOP_MARGIN = "TopMargin";
		public const string NEW_WIDTH = "NewWidth";
		public const string NEW_HEIGHT = "NewHeight";

		const int ALPHAANIMATIONTIME = 100;
		const int FRAMEANIMATIONTIME = 200;

		public const int DefaultShadowSize = 10;

		protected Frame frame;
		Color backgroundColor;
		Color borderColor;
		bool hidden;

		UIView leftBorder, rightBorder, topBorder, bottomBorder;

		public virtual Frame Frame {
			get {
				if (frame == null) {
					return new Frame ();
				}
				return frame;
			} set {
				frame = value;

				RelativeLayout.LayoutParams parameters = new RelativeLayout.LayoutParams(value.W, value.H);
				parameters.LeftMargin = value.X;
				parameters.TopMargin = value.Y;

				LayoutParameters = parameters;

				LayoutSubviews ();
			}
		}
			
		public virtual Color BackgroundColor {
			get {
				return backgroundColor;
			} set {
				backgroundColor = value;
				SetBackgroundColor (backgroundColor);
			}
		}

		public bool HasParent {
			get {
				return Parent != null;		
			}
		}

		public Rect HitRect {
			get {
				Rect cellRect = new Rect ();
				GetHitRect (cellRect);

				return cellRect;
			}
		}

		public Color BorderColor {
			get {
				return borderColor;		
			}
			set {
				borderColor = value;

				if (leftBorder != null) {
					leftBorder.BackgroundColor = borderColor;
				}
				if (rightBorder != null) {
					rightBorder.BackgroundColor = borderColor;
				}
				if (topBorder != null) {
					topBorder.BackgroundColor = borderColor;
				}
				if (bottomBorder != null) {
					bottomBorder.BackgroundColor = borderColor;
				}
			}
		}

		public bool Hidden {
			get {
				return hidden;
			} set {
				hidden = value;

				if (hidden) {
					Hide ();
				} else {
					Show ();
				}
			}
		}

		public ObjectAnimator AlphaOutAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", Alpha, 0);
			}
		}

		public ObjectAnimator AlphaInAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", Alpha, 1);
			}
		}

		public UIView (Activity context) : base (context)
		{

		}

		public UIView (IntPtr a, Android.Runtime.JniHandleOwnership b) : base (a, b)
		{
			
		}

		public virtual void LayoutSubviews ()
		{
			
		}

		[Export]
		public float getAlpha ()
		{
			return Alpha;	
		}

		[Export]
		public void setAlpha (float alpha)
		{
			Alpha = alpha;
		}

		[Export]
		public float getTopMargin ()
		{
			return Frame.Y;
		}

		[Export]
		public void setTopMargin (float margin)
		{
			Frame = new Frame (Frame.X, (int)margin, Frame.W, Frame.H);
			RequestLayout();
		}

		[Export]
		public float getLeftMargin ()
		{
			return Frame.X;
		}

		[Export]
		public void setLeftMargin (float margin)
		{
			Frame = new Frame ((int)margin, Frame.Y, Frame.W, Frame.H);
			RequestLayout();
		}

		[Export]
		public int getNewWidth ()
		{
			return Frame.W;
		}

		[Export]
		public void setNewWidth (int width)
		{
			Frame = new Frame (frame.X, Frame.Y, width, Frame.H);
			RequestLayout();
		}

		[Export]
		public int getNewHeight ()
		{
			return Frame.H;
		}

		[Export]
		public void setNewHeight (int height)
		{
			Frame = new Frame (frame.X, Frame.Y, Frame.W, height);
			RequestLayout();
		}	

		public int SizeHeightToFit ()
		{
			return SizeHeightToFitWithMin (0);
		}

		public int SizeHeightToFitWithMin (int min)
		{
			Measure (0, 0);

			int height = MeasuredHeight;

			if (MeasuredHeight < min) {
				height = min;
			}

			Frame = new Frame (frame.X, frame.Y, frame.W, height);
			return height;
		}

		public void SetSlightlyRoundWithBackgroundColor (Color color)
		{
			GradientDrawable background = new GradientDrawable ();
			background.SetCornerRadius (Frame.H / 10);
			background.SetColor (color);

			Background = background;

			RemoveBorders ();
		}

		public void MakeIntoCircleWithColor (Color color)
		{
			GradientDrawable background = new GradientDrawable ();
			background.SetCornerRadius (Frame.W / 2);
			background.SetColor (color);

			Background = background;

			RemoveBorders ();
		}

		public void SetMultiColorBackground (int[] colours)
		{
			GradientDrawable drawable = new GradientDrawable (GradientDrawable.Orientation.LeftRight, colours);
			Background = drawable;
		}

		public void AddBorders (Activity context, bool left, bool right, bool top, bool bottom)
		{
			if (left) {
				leftBorder = new UIView (context) { BackgroundColor = borderColor };
				AddView (leftBorder);
			}

			if (right) {
				rightBorder = new UIView (context) { BackgroundColor = borderColor };
				AddView (rightBorder);
			}

			if (top) {
				topBorder = new UIView (context) { BackgroundColor = borderColor };
				AddView (topBorder);
			}

			if (bottom) {
				bottomBorder = new UIView (context) { BackgroundColor = borderColor };
				AddView (bottomBorder);
			}
		}

		public void SetBorderFrames (int w)
		{
			if (leftBorder != null) {
				leftBorder.Frame = new Frame (0, 0, w, frame.H);
			}

			if (rightBorder != null) {
				rightBorder.Frame = new Frame (frame.W - w, 0, w, frame.H);
			}

			if (topBorder != null) {
				topBorder.Frame = new Frame (0, 0, frame.W, w);
			}

			if (bottomBorder != null) {
				bottomBorder.Frame = new Frame (0, frame.H - w, frame.W, w);
			}
		}

		public void RemoveBorders ()
		{
			RemoveView (leftBorder);
			RemoveView (rightBorder);
			RemoveView (topBorder);
			RemoveView (bottomBorder);
		}

		public void AddBottomShadow ()
		{
			AddBottomShadow (DefaultShadowSize);
		}

		public void AddLeftAndBotShadow ()
		{
			AddLeftAndBotShadow (DefaultShadowSize);
		}

		public void AddLeftAndBotShadow (int size)
		{
			AddShadow (size, 1, 1, size);
		}

		public void AddBottomShadow (int size)
		{
			AddShadow (0, 0, 0, size);
		}

		public void AddShadow (int leftSize, int topSize, int rightSize, int bottomSize)
		{
			RectShape shape2 = new RectShape ();
			ShapeDrawable shapeDrawable = new ShapeDrawable (shape2);

			shapeDrawable.Paint.AntiAlias = true;
			shapeDrawable.Paint.SetStyle (Paint.Style.Fill);
			shapeDrawable.Paint.Color = BackgroundColor;
			shapeDrawable.Paint.SetShadowLayer (bottomSize, 0, 0, Color.Black);

			SetLayerType (Android.Views.LayerType.Software, null);

			LayerDrawable layerDrawable = new LayerDrawable (new Drawable[] { shapeDrawable  });
			layerDrawable.SetLayerInset (0, leftSize, topSize, rightSize, bottomSize);

			Background = layerDrawable;
		}

		public void AnimateHide (Action completed)
		{
			ObjectAnimator animator = AlphaOutAnimator;
			animator.SetDuration (ALPHAANIMATIONTIME);

			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (delegate {
					completed ();
					Visibility = Android.Views.ViewStates.Gone;
				});
			};	
		}

		public void AnimateShow (Action completed)
		{
			ObjectAnimator animator = AlphaInAnimator;

			animator.SetDuration (ALPHAANIMATIONTIME);
			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (delegate {
					completed ();
					Visibility = Android.Views.ViewStates.Visible;
				});
			};
		}

		public void AnimateY (int y)
		{
			ObjectAnimator xAnim = ObjectAnimator.OfFloat (this, TOP_MARGIN, Frame.Y, y);

			xAnim.SetDuration (FRAMEANIMATIONTIME);

			xAnim.Start ();
		}

		public void AnimateX (Frame newFrame)
		{
			ObjectAnimator xAnim = ObjectAnimator.OfFloat (this, LEFT_MARGIN, Frame.X, newFrame.X);

			xAnim.SetDuration (FRAMEANIMATIONTIME);

			xAnim.Start ();
		}

		public void AnimateWidth (Frame newFrame, Action completed)
		{
			ObjectAnimator wAnim = ObjectAnimator.OfInt (this, NEW_WIDTH, Frame.W, newFrame.W);

			wAnim.SetDuration (FRAMEANIMATIONTIME);

			wAnim.Start ();

			wAnim.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public void AnimateXAndWidth (Frame newFrame, Action completed)
		{
			ObjectAnimator xAnim = ObjectAnimator.OfFloat (this, LEFT_MARGIN, (float)Frame.X, (float)newFrame.X);
			ObjectAnimator wAnim = ObjectAnimator.OfInt (this, NEW_WIDTH, Frame.W, newFrame.W);

			AnimatorSet set = new AnimatorSet ();
			set.SetDuration (FRAMEANIMATIONTIME);
			set.PlayTogether (new ObjectAnimator[] { xAnim, wAnim });

			set.Start ();

			set.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public void AnimateXWidthAndHeight (Frame newFrame, Action completed)
		{
			ObjectAnimator xAnim = ObjectAnimator.OfFloat (this, LEFT_MARGIN, (float)Frame.X, (float)newFrame.X);
			ObjectAnimator wAnim = ObjectAnimator.OfInt (this, NEW_WIDTH, Frame.W, newFrame.W);
			ObjectAnimator hAnim = ObjectAnimator.OfInt (this, NEW_HEIGHT, Frame.H, newFrame.H);

			AnimatorSet set = new AnimatorSet ();
			set.SetDuration (FRAMEANIMATIONTIME);
			set.PlayTogether (new ObjectAnimator[] { xAnim, wAnim, hAnim });

			set.Start ();

			set.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		public virtual void Hide ()
		{
			Alpha = 0;
			Visibility = Android.Views.ViewStates.Gone;
		}

		public virtual void Show ()
		{
			Alpha = 1;
			Visibility = Android.Views.ViewStates.Visible;
		}

		public void UpdateY (int y)
		{
			Frame = new Frame (frame.X, y, frame.W, frame.H);
		}

		public void UpdateX (int x)
		{
			Frame = new Frame (x, frame.Y, frame.W, frame.H);
		}

		public void UpdateHeight (int height)
		{
			Frame = new Frame (frame.X, frame.Y, frame.W, height);
		}
			
		public void AddViews (params object[] items)
		{
			foreach (object item in items) {
				AddView (item as Android.Views.View);
			}
		}

		public void RemoveViews (params object[] items)
		{
			foreach (object item in items) {
				RemoveView (item as Android.Views.View);
			}
		}	

		public void UpdateFrameBy (int x, int y, int w, int h)
		{
			Frame = new Frame (frame.X + x, frame.Y + y, frame.W + w, frame.H + h);
		}

		public override void AddView (Android.Views.View child)
		{
			try {
				base.AddView (child);
			} catch {
				Console.WriteLine ("!!!! Caught exception: Failed to add View: " + child.GetType ());
			}
		}

		public void Round ()
		{
			SetCornerRadius (Frame.W / 2);
		}

		public void SetCornerRadius (float radius)
		{
			GradientDrawable background = new GradientDrawable ();
			background.SetCornerRadius (radius);
			background.SetColor (BackgroundColor);

			Background = background;
		}

	}
}

