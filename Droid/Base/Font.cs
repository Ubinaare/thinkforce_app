﻿using System;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public enum FontStyle
	{
		Serif,
		SerifLight,
		SerifThin,
		SerifCondensed
	}

	public class Font
	{
		public static Dictionary<int, string> EnumStringValues = new Dictionary<int, string> {
			{ 0, "sans-serif" },
			{ 1, "sans-serif-light" },
			{ 2, "sans-serif-thin" },
			{ 3, "sans-serif-condensed" },
		};

		public static Font Get (FontStyle style, int size, bool bold = false, bool italic = false)
		{
			return new Font () { Name = EnumStringValues[(int)style], Size = size, Bold = bold, Italic = italic };
		}

		public string Name { get; set; }

		public int Size { get; set; }

		public bool Bold { get; set; }

		public bool Italic { get; set; }
	}
}

