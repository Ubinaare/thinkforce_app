﻿using System;
using Android.Content.Res;
using Android.App;

namespace thinkforce_app.Droid
{
	public class Device
	{
		public static bool Measured { get; set; }

		const string ActivityService = "activity";

		public static int ScreenWidth { get; internal set; }

		public static int ScreenHeight { get; internal set; }

		public static int ExtensionHeight {
			get {
				if (IsTablet) {
					return NavigationBarExtensionHeightWithShadow + StatusBarHeight;
				} else {
					return NavigationBarExtensionHeightWithShadow;
				}
			}	
		}

		public static int NavigationShadowHeight {
			get {
				return UIView.DefaultShadowSize;		
			}
		}

		public static int NavigationBarHeight { get; internal set; }

		public static int NavigationBarExtensionHeightWithShadow {
			get {
				return NavigationBarHeight;
			}
		}

		public static int NavigationBarExtensionHeightWithoutShadow {
			get {
				return NavigationBarHeight - UIView.DefaultShadowSize;
			}
		}

		public static int NavigationBarHeightWithShadow {
			get {
				return NavigationBarHeight + UIView.DefaultShadowSize;
			}
		}

		public static int MainListHeight {
			get {
				return ScreenHeight - (NavigationBarHeight + ExtensionHeight);
			}
		}

		public static int TaskListHeight {
			get {
				return ScreenHeight - (NavigationBarHeight + ExtensionHeight);
			}
		}

		public static int TrueScreenHeight {
			get {
				return ScreenHeight - (NavigationBarHeight + StatusBarHeight);
			}
		}

		public static int StatusBarHeight { get; internal set; }

		public static string DeviceName { get; internal set; }

		public static bool IsMotorola { get; internal set; }

		public static bool IsLG { get; internal set; }

		public static bool IsSony { get; internal set; }

		public static long Memory { get; internal set; }

		public static bool IsTablet { get; internal set; }

		public static int MenuWidth {
			get {
				return (int)(ScreenWidth * 0.66);		
			}
		}

		public static string OSVersion {
			get {
				return Android.OS.Build.VERSION.Release;
			}
		}

		public static bool IsLollipopOrHigher {
			get {
				return Android.OS.Build.VERSION.SdkInt >= Android.OS.BuildVersionCodes.Lollipop;
			}
		}

		public static bool IsWideScreen {
			get {
				return (float)((float)ScreenHeight / (float)ScreenWidth) < 1.7;
			}
		}

		public static void Measure (Activity context)
		{
			Measured = true;

			Resources resources = context.Resources;

			ScreenWidth = resources.DisplayMetrics.WidthPixels;

			ScreenHeight = resources.DisplayMetrics.HeightPixels;

			int navBarId = resources.GetIdentifier ("navigation_bar_height", "dimen", "android");
			int statusbarId = resources.GetIdentifier ("status_bar_height", "dimen", "android");

			NavigationBarHeight = resources.GetDimensionPixelSize (navBarId);

			StatusBarHeight = resources.GetDimensionPixelSize (statusbarId);

			ActivityManager manager = (ActivityManager)context.GetSystemService (ActivityService);
			Android.App.ActivityManager.MemoryInfo outInfo = new ActivityManager.MemoryInfo ();
			manager.GetMemoryInfo (outInfo);
			Memory = outInfo.TotalMem;

			DeviceName = GetDeviceName ();
			IsMotorola = DeviceName.Contains ("Motorola");
			IsLG = DeviceName.Contains ("LGE");
			IsSony = DeviceName.Contains ("Sony");

			IsTablet = GetIsTablet (context);
		}

		static bool GetIsTablet (Activity context)
		{
			// I have no idea what is going on here, but it seems to work
			return (context.Resources.Configuration.ScreenLayout & ScreenLayout.SizeMask) >= ScreenLayout.SizeLarge;
		}

		public static string GetDeviceName () 
		{
			String manufacturer = Android.OS.Build.Manufacturer;
			String model = Android.OS.Build.Model;

			if (model.StartsWith (manufacturer)) {
				return Capitalize (model);
			}

			return Capitalize (manufacturer) + " " + model;
		}

		private static String Capitalize (string str)
		{
			if (Android.Text.TextUtils.IsEmpty (str)) {
				return str;
			}

			char[] arr = str.ToCharArray ();
			bool capitalizeNext = true;
			string phrase = "";

			foreach (char c in arr) {
				if (capitalizeNext && Java.Lang.Character.IsLetter (c)) {
					phrase += Java.Lang.Character.ToUpperCase (c);
					capitalizeNext = false;
					continue;
				} else if (Java.Lang.Character.IsWhitespace (c)) {
					capitalizeNext = true;
				}

				phrase += c;
			}

			return phrase;
		}
	}
}

