﻿using System;
using Android.Widget;
using Android.Graphics;
using Android.App;
using Android.Animation;
using Java.Interop;
using Android.Views.Animations;
using Android.OS;
using Android.Graphics.Drawables;

namespace thinkforce_app.Droid
{
	public class UIImageView : ImageView
	{
		const int ALPHAANIMATIONTIME = 100;

		Frame frame;
		Color backgroundColor;
		int imageResource;

		public bool IsHidden {
			get {
				return Math.Abs (Alpha) < 0.01f;
			}
		}

		public Frame Frame {
			get {
				return frame;
			}
			set {
				frame = value;
				LayoutParameters = LayoutUtils.GetRelative (frame.X, frame.Y, frame.W, frame.H);
				LayoutSubviews ();
			}
		}

		public Color BackgroundColor {
			get {
				return backgroundColor;
			}
			set {
				backgroundColor = value;
				SetBackgroundColor (backgroundColor);
			}
		}

		public bool HasImageResource {
			get {
				return imageResource != 0;
			}
		}

		public int ImageResource {
			get {
				return imageResource;
			}
			set {
				imageResource = value;
				SetImageResource (imageResource);
			}
		}

		public ObjectAnimator AlphaOutAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", Alpha, 0);
			}
		}

		public ObjectAnimator AlphaInAnimator {
			get {
				return ObjectAnimator.OfFloat (this, "Alpha", Alpha, 1);
			}
		}

		public UIImageView (Activity context) : base (context)
		{
			
		}

		public virtual void LayoutSubviews ()
		{
			
		}

		public void FitImageToFrame ()
		{
			SetScaleType (Android.Widget.ImageView.ScaleType.CenterInside);	
		}

		[Export]
		public float getAlpha ()
		{
			return Alpha;	
		}

		[Export]
		public void setAlpha (float alpha)
		{
			Alpha = alpha;
		}

		public virtual void Hide ()
		{
			Alpha = 0;
			Visibility = Android.Views.ViewStates.Gone;
		}

		public virtual void Show ()
		{
			Alpha = 1;
			Visibility = Android.Views.ViewStates.Visible;
		}

		public void AnimateHide (Action completed)
		{
			ObjectAnimator animator = AlphaOutAnimator;
			animator.SetDuration (ALPHAANIMATIONTIME);

			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (delegate {
					Visibility = Android.Views.ViewStates.Gone;
					completed ();
				});
			};	
		}

		public void AnimateShow (Action completed)
		{
			Visibility = Android.Views.ViewStates.Visible;

			ObjectAnimator animator = AlphaInAnimator;
			animator.SetDuration (ALPHAANIMATIONTIME);

			animator.Start ();

			animator.AnimationEnd += delegate {
				new Handler ().Post (completed);
			};
		}

		bool rotating;

		public void AnimateInfiniteRotation ()
		{
			if (!rotating) {

				rotating = true;

				RotateAnimation animation = new RotateAnimation (0, 359, Dimension.RelativeToSelf, 0.5f, Dimension.RelativeToSelf, 0.5f);

				animation.RepeatCount = Animation.Infinite;
				animation.Duration = 2000;
				animation.FillAfter = true;

				Console.WriteLine ("Rotation Start");

				StartAnimation (animation);
			}
		}

		public void Round ()
		{
			SetCornerRadius (Frame.W / 2);
		}

		public void SetCornerRadius (float radius)
		{
			GradientDrawable background = new GradientDrawable ();
			background.SetCornerRadius (radius);
			background.SetColor (BackgroundColor);

			Background = background;
		}

	}
}

