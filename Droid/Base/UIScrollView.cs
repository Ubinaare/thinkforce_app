﻿using System;
using Android.Widget;
using Android.App;
using Android.Graphics;

namespace thinkforce_app.Droid
{
	public class UIScrollView : ScrollView
	{
		UIView child;

		Frame frame;
		Android.Graphics.Color backgroundColor;

		public bool IsDragInProgress { get; internal set; }

		public UIView ScrollView {
			get {
				return child;
			}
		}

		public Rect HitRect {
			get {
				Rect rect = new Rect ();

				rect.Top = ScrollY;
				rect.Bottom = ScrollY + Height;
				rect.Left = 0;
				rect.Right = Width;

				return rect;
			}
		}

		public virtual Frame Frame {
			get {
				return frame;		
			}
			set {
				frame = value;
				LayoutParameters = LayoutUtils.GetRelative (frame.X, frame.Y, frame.W, frame.H);
				Console.WriteLine ("Scrollview Frame: " + Frame);
				LayoutSubviews ();
			}
		}

		public virtual Android.Graphics.Color BackgroundColor {
			get {
				return backgroundColor;
			}
			set {
				backgroundColor = value;

				SetBackgroundColor (backgroundColor);
			}
		}

		public int ContentHeight {
			get {
				return child.Height;
			}
		}
			
		public UIScrollView (Activity context) : base (context)
		{
			VerticalScrollBarEnabled = false;
			HorizontalScrollBarEnabled = false;

			child = new UIView (context);

			base.AddView (child);
		}

		public virtual void LayoutSubviews ()
		{
			
		}

		public override bool OnTouchEvent (Android.Views.MotionEvent e)
		{
			if (e.Action == Android.Views.MotionEventActions.Up || e.Action == Android.Views.MotionEventActions.Cancel) {
				IsDragInProgress = false;
			}

			return base.OnTouchEvent (e);
		}

		public override bool OnInterceptTouchEvent (Android.Views.MotionEvent ev)
		{
			if (ev.Action == Android.Views.MotionEventActions.Down) {
				IsDragInProgress = true;
			}

			return base.OnInterceptTouchEvent (ev);
		}

		public void AddViews (params object[] items)
		{
			foreach (object item in items) {
				AddView (item as Android.Views.View);
			}
		}

		public override void AddView (Android.Views.View child)
		{
			this.child.AddView (child);
		}

		public override void RemoveView (Android.Views.View child)
		{
			this.child.RemoveView (child);
		}

		public override void RemoveAllViews ()
		{
			this.child.RemoveAllViews ();
		}

		public void ScrollToY (int y) 
		{
			ScrollTo (ScrollX, y);	
		}

		public void ScrollToYAnimated (int y)
		{
			SmoothScrollTo (ScrollX, y);
		}

		public void StopScroll ()
		{
			SmoothScrollTo (ScrollX, ScrollY);
		}
	}
}

