﻿using System;
using Android.Widget;
using Android.App;

namespace thinkforce_app.Droid
{
	public class UIListView : ListView
	{
		Frame frame;

		public virtual Frame Frame {
			get {
				if (frame == null) {
					return new Frame ();
				}
				return frame;
			} set {
				frame = value;

				RelativeLayout.LayoutParams parameters = new RelativeLayout.LayoutParams(value.W, value.H);
				parameters.LeftMargin = value.X;
				parameters.TopMargin = value.Y;

				LayoutParameters = parameters;

				LayoutSubviews ();
			}
		}

		public UIListView (Activity context) : base (context)
		{
			
		}

		public virtual void LayoutSubviews ()
		{
			
		}

	}
}

