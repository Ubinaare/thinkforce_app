﻿using System;

namespace thinkforce_app.Droid
{
	public class Response
	{
		public const string Error = "Error!";

		public const int Ok = 1;
		public const int InvalidCredentials = 2;
		public const int AccountClosed = 3;
		public const int SignUpFirst = 4;
		public const int EndDateMustBeHigher = 5;
		public const int SuccessorCannotStart = 6;
		public const int ReplaceFailure = 7;

		public const int FoundLocalData = 667;
		public const int NoLocalData = 668;

		public const int ErrorCreatingStream = -6;
		public const int ErrorWritingStream = -7;
		public const int ErrorGettingResponse = -8;
		public const int ErrorParsingResponse = -9;

		public const int BadRequest = 400;
		public const int UnknownError = -1000;
		public const int NoNetwork = -1001;

		public Response ()
		{
		}
	}
}

