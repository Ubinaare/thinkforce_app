﻿using System;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public class LampStateResponse : Response
	{
		public List<LampState> States { get; set; }

		public LampStateResponse ()
		{
			States = new List<LampState> { };
		}
	}
}

