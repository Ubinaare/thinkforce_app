﻿using System;
using System.Json;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using System.Text;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public class Networking
	{
		const string LIGHTS_BASE = "http://192.168.37.241/api/74911eed7a0f0f48641a562561102c9c/";
		const string LIGHTS_API = "lights/";
		const string LIGHTS_GROUP_API = "groups/0/action/";

		const string WHEELCHAIRBASE = "http://192.168.37.157/cgi-bin/";
		const string WHEELFORWARDAPI = "forward";
		const string WHEELREVERSEAPI = "reverse";
		const string WHEELSTOPAPI = "stop";
		const string WHEELLEFTAPI = "left";
		const string WHEELRIGHTAPI = "right";

		const int TIMEOUT_IN_MILLISECONDS = (int)(30 * 1000);

		const string NAMERESOLUTIONFAILURE = "NameResolutionFailure";
		const string CONNECTFAILURE = "ConnectFailure";

		const string COULDNOTCONNECT = "Could not reach the server. ";
		const string INTERNETAVAILABILITY = "Make sure you are connected to the internet";
		const string ERRORPARSINGRESPONSE = "Error parsing network response. Please try again";

		public static async void TurnLightsOn ()
		{
			await TurnLights (true);
		}

		public static async void TurnLightsOff () 
		{
			await TurnLights (false);
		}

		public static async Task<List<LampState>> GetLampStates ()
		{
			JsonValue result = await Get (LIGHTS_BASE, LIGHTS_API, null);
			Console.WriteLine ("Lamp States: " + result);

			List<LampState> states = Codec.DecodeLampStates (result);

			return states;
		}

		async static Task<JsonValue> TurnLights (bool _on)
		{
			JsonValue json = Codec.TurnLights (_on);

//			JsonValue result = await Put (LIGHTS_BASE, LIGHTS_API + "1/state", json, null);
			JsonValue result = await Put (LIGHTS_BASE, LIGHTS_GROUP_API, json, null);

			return result;
		}

		public async static Task<JsonValue> TurnLightOfIndex (int index, bool _on)
		{
			JsonValue json = Codec.TurnLights (_on);

			JsonValue result = await Put (LIGHTS_BASE, LIGHTS_API + (index + 1) + "/state", json, null);

			Console.WriteLine (result.ToString ());

			return result;
		}

		public async static Task<JsonValue> TurnColorOfLampOfIndex(int index, LampColor color)
		{
			JsonValue json = Codec.EncodeHueChange (color);

			JsonValue result = await Put (LIGHTS_BASE, LIGHTS_API + (index + 1) + "/state", json, null);

			Console.WriteLine (result.ToString ());

			return result;
		}

		public static async void DriveChair (MovementDirection direction)
		{
			string api = GetWheelchairApi (direction);
			Console.WriteLine ("Api: " + api);

			if (api == null) {
				return;
			}

			await Get (WHEELCHAIRBASE, api, null);
		}

		static string GetWheelchairApi (MovementDirection direction)
		{
			if (direction == MovementDirection.Forward) {
				return WHEELFORWARDAPI;
			} else if (direction == MovementDirection.Reverse) {
				return WHEELREVERSEAPI;
			} else if (direction == MovementDirection.Stop) {
				return WHEELSTOPAPI;
			} else if (direction == MovementDirection.Left) {
				return WHEELLEFTAPI;
			} else if (direction == MovementDirection.Right) {
				return WHEELRIGHTAPI;
			}

			return null;
		}

		public static void DriveBB (MovementDirection direction)
		{
			Client.Instance.Publish (new SendData { Message = GetBB8Message(direction) });
		}

		static string GetBB8Message (MovementDirection direction)
		{
			if (direction == MovementDirection.Forward) {
				return WHEELFORWARDAPI;
			} else if (direction == MovementDirection.Reverse) {
				return WHEELREVERSEAPI;
			} else if (direction == MovementDirection.Stop) {
				return WHEELSTOPAPI;
			} else if (direction == MovementDirection.Left) {
				return WHEELLEFTAPI;
			} else if (direction == MovementDirection.Right) {
				return WHEELRIGHTAPI;
			}

			return null;
		}

		static async Task<JsonValue> Get (string BASE, string API, Cookie cookie)
		{
			JsonObject result = new JsonObject ();
			Console.WriteLine("GET: " + BASE + API);

			try {
				Uri uri = new Uri (BASE + API);

				HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create (uri);

				request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;

				request.Method = "GET";

				if (cookie != null) {
					CookieContainer cookieContainer = new CookieContainer();
					cookie.Domain = uri.Host;
					cookieContainer.Add(cookie);
					request.CookieContainer = cookieContainer;
				}

				HttpWebResponse response = (HttpWebResponse)await request.GetResponseAsync ().WithTimeout (TIMEOUT_IN_MILLISECONDS);

				string returnString;

				try {
					var stream = response.GetResponseStream ();
					returnString = StreamToString (stream);
				} catch (Exception e) {
					returnString = "Exception: " + e;
				}

				try {
					return JsonValue.Parse (returnString);
				} catch (Exception e) {
					result [Codec.EXCEPTION] = Response.ErrorParsingResponse;
					result [Codec.MESSAGE] = e.Message;
					return result;
				}

			} catch (Exception e) {
				Console.WriteLine ("Get Exception2: " + e);
				return null;
			}
		}

		async static Task<JsonValue> Put (string baseUrl, string api, JsonValue json, Cookie cookie)
		{
			return await SendData ("PUT", baseUrl, api, json, cookie);
		}

		async static Task<JsonValue> Post (string baseUrl, string api, JsonValue json)
		{
			return await SendData ("POST", baseUrl, api, json);
		}

		async static Task<JsonValue> SendData (string method, string baseUrl, string api, JsonValue json, Cookie cookie = null)
		{
			JsonObject result = new JsonObject ();
			Stream dataStream;
			HttpWebResponse response;

			byte[] byteArray = null;

			if (json != null) {
				byteArray = Encoding.UTF8.GetBytes (json.ToString ());
			} else {
				byteArray = new byte[0];
			}

			string url = baseUrl + api;
			Uri uri = new Uri (url);

			HttpWebRequest request = (HttpWebRequest)HttpWebRequest.Create (uri);

			if (json != null) {
				request.ContentType = "application/json";
			}

			request.Method = method;
			request.ContentLength = byteArray.Length;
			request.Timeout = TIMEOUT_IN_MILLISECONDS;

			if (request.Method == "POST") {
				request.AllowAutoRedirect = false;
				if (json != null) {
					request.Accept = "application/json";
				}
			}

			if (cookie == null) {
				request.CookieContainer = new CookieContainer ();
				request.CookieContainer.Add (uri, new CookieCollection ());
			} else {
				CookieContainer cookieContainer = new CookieContainer();
				cookie.Domain = uri.Host;
				cookieContainer.Add(cookie);
				request.CookieContainer = cookieContainer;
			}

			try {
				dataStream = await request.GetRequestStreamAsync ().WithTimeout(TIMEOUT_IN_MILLISECONDS);
			} catch (Exception e) {
				result [Codec.EXCEPTION] = Response.ErrorCreatingStream;
				if (IsNameResolutionFailure (e) || IsNetworkingUnreachable (e)) {
					result [Codec.MESSAGE] = COULDNOTCONNECT + INTERNETAVAILABILITY;
				} else {
					result [Codec.MESSAGE] = e.Message;
				}

				return result;
			}

			try {
				if (json != null) {
					await dataStream.WriteAsync (byteArray, 0, byteArray.Length);
				}
			} catch (Exception e) {
				result [Codec.EXCEPTION] = Response.ErrorWritingStream;

				if (dataStream == null) {
					result [Codec.MESSAGE] = "Failed to establish connection. Please make sure you're connected to the internet";
				} else {
					result [Codec.MESSAGE] = e.Message;
				}
				return result;
			}

			try {
				response = (HttpWebResponse)await request.GetResponseAsync ().WithTimeout (TIMEOUT_IN_MILLISECONDS);
			} catch (WebException e) {
				response = (HttpWebResponse)e.Response;
				int statusCode = 32000;
				string message = "";
				if (response != null) {
					statusCode = (int)response.StatusCode;
					message = StreamToString (response.GetResponseStream ());
				}
				result [Codec.EXCEPTION] = statusCode;
				result [Codec.MESSAGE] = message;
				return result;
			} catch (Exception e) {
				result [Codec.EXCEPTION] = Response.ErrorGettingResponse;
				result [Codec.MESSAGE] = e.Message;
				return result;
			}

			if (response == null) {
				result [Codec.EXCEPTION] = Response.ErrorGettingResponse;
				return result;
			}

			if (response.StatusCode != HttpStatusCode.OK) {
				result [Codec.EXCEPTION] = ((int)response.StatusCode).ToString ();
				result [Codec.MESSAGE] = response.StatusDescription;

				return result;
			}

			var stream = response.GetResponseStream ();
			var returnValue = StreamToString (stream);

			try {
				JsonValue parsedJson = JsonObject.Parse (returnValue);

				if (cookie == null) {
					foreach (Cookie responseCookie in response.Cookies) {
						parsedJson[responseCookie.Name] = responseCookie.Value;
					}
				}

				return parsedJson;
			} catch {
				result [Codec.EXCEPTION] = Response.ErrorParsingResponse;
				result [Codec.MESSAGE] = ERRORPARSINGRESPONSE;
				return result;
			}
		}

		static bool ContainsInnerException (Exception e)
		{
			if (e.InnerException == null) {
				return false;
			}

			if (e.InnerException.Message == null) {
				return false;
			}

			return true;
		}

		static bool IsNameResolutionFailure (Exception e)
		{
			if (!ContainsInnerException (e)) {
				return false;
			}

			return e.InnerException.Message.Contains (NAMERESOLUTIONFAILURE);
		}

		static bool IsNetworkingUnreachable (Exception e)
		{
			if (!ContainsInnerException (e)) {
				return false;
			}

			return e.InnerException.Message.Contains (CONNECTFAILURE);
		}

		public static string StreamToString (Stream stream)
		{
			using (StreamReader reader = new StreamReader (stream, Encoding.UTF8)) {
				return reader.ReadToEnd ();
			}
		}


	}
}

