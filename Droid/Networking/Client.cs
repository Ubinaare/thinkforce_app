﻿using System;
using PubNubMessaging.Core;
using System.Json;

namespace thinkforce_app.Droid
{
	public class Client
	{
		const string PUBLISHKEY = "pub-c-98199bc8-0eff-41df-8b3c-4df4a3a6d1f8";
		const string SUBSCRIBEKEY = "sub-c-ca87811c-f7ea-11e5-b552-02ee2ddab7fe";

		const string CHANNEL = "garagebb8control";

		private Pubnub _pubnub;

		private bool subscribed;

		public static readonly Client Instance = new Client ();

		private Client ()
		{
			
		}

		public void Subscribe (Action<SendData> messageReceived)
		{
			if (subscribed) {
				Console.WriteLine ("Already subscribed");
				return;
			}
			if (_pubnub != null) {
				Console.WriteLine ("Already subscribing");
				return;
			}
			_pubnub = new Pubnub (PUBLISHKEY, SUBSCRIBEKEY);
			_pubnub.Subscribe<string> (
				CHANNEL,
				delegate (string message) {
					try {
						if (messageReceived != null) {
							JsonValue json = JsonValue.Parse (message);
							SendData data = new SendData { Message = json.ToString() };
							messageReceived (data);
						}
					} catch (Exception e) {
						Console.WriteLine ("Unexpected exception: " + e.Message);
						Console.WriteLine (e.StackTrace);
					}
				},
				delegate (string message) {
					Console.WriteLine ("pubnub connect: " + message);
					subscribed = true;
				},
				delegate (PubnubClientError err) {
					Console.WriteLine ("pubnub error: " + err.Description);
				}
			);
		}

		public void Unsubscribe ()
		{
			_pubnub.EndPendingRequests ();
			_pubnub.Unsubscribe<string>(
				CHANNEL,
				delegate {
					try {
						_pubnub = null;
						subscribed = false;
					} catch (Exception e) {
						Console.WriteLine ("Unexpected exception: " + e.Message);
						Console.WriteLine (e.StackTrace);
					}
				},
				delegate {
					try {
						_pubnub = null;
						subscribed = false;
					} catch (Exception e) {
						Console.WriteLine ("Unexpected exception: " + e.Message);
						Console.WriteLine (e.StackTrace);
					}
				},
				delegate {
					try {
						_pubnub = null;
						subscribed = false;
					} catch (Exception e) {
						Console.WriteLine ("Unexpected exception: " + e.Message);
						Console.WriteLine (e.StackTrace);
					}
				},
				delegate {
					try {
						_pubnub = null;
						subscribed = false;
					} catch (Exception e) {
						Console.WriteLine ("Unexpected exception: " + e.Message);
						Console.WriteLine (e.StackTrace);
					}
				}
			);
		}

		public void Publish (SendData data)
		{
			Publish (new JsonObject {
				{"message", data.Message},
			});
		}

		private void Publish (JsonValue json)
		{
			if (_pubnub == null) {
				Console.WriteLine ("pubnub not subscribed, not publishing message");
				return;
			}
			if (!subscribed) {
				Console.WriteLine ("pubnub subscribe in progress, not publishing message");
				return;
			}
			_pubnub = new Pubnub (PUBLISHKEY, SUBSCRIBEKEY);
			string message = json.ToString ();
			Console.WriteLine ("publishing message " + message);
			_pubnub.Publish (
				CHANNEL,
				message,
				delegate {
					Console.WriteLine ("publishing message successful");
				}, 
				delegate {
					Console.WriteLine ("publishing message failed");
				}
			);
		}
	}
}

