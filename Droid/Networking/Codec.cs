﻿using System;
using System.Json;
using System.Collections.Generic;

namespace thinkforce_app.Droid
{
	public class Codec
	{
		public const string EXCEPTION = "exception";
		public const string MESSAGE = "message";
		public const string STATUS = "status";

		public static JsonValue TurnLights (bool _on)
		{
			return new JsonObject { { "on", _on } };
		}

		public static JsonValue EncodeHueChange (LampColor color)
		{
			return new JsonObject { 
				{ "hue", (int)color },
				{ "sat", 254 },
				{ "bri", 150 }
			};
		}

		public static List<LampState> DecodeLampStates (JsonValue json)
		{
			List<LampState> states = new List<LampState> ();
			Console.WriteLine ("Count: " + json.Count);
			foreach (JsonValue child in json) {
				Console.WriteLine ("InLoop");
				LampState state = DecodeLampState (child);
				states.Add (state);
			}

			return states;
		}

		public static LampState DecodeLampState (JsonValue json)
		{
			LampState state = new LampState ();

			state.Name = GetStringValue (json, "name", "");

			state.IsOn = GetBoolValue (json ["state"], "on", false);

			return state;
		}

		static int GetIntValue (JsonValue json, string key, int defaultValue)
		{
			if (!json.ContainsKey (key)) {
				return defaultValue;
			}

			if (json [key] == null) {
				return defaultValue;
			}

			return json [key];
		}

		static string GetStringValue (JsonValue json, string key, string defaultValue)
		{
			if (!json.ContainsKey (key)) {
				return defaultValue;
			}

			if (json [key] == null) {
				return defaultValue;
			}

			return json [key];
		}

		static bool GetBoolValue (JsonValue json, string key, bool defaultValue)
		{
			if (!json.ContainsKey (key)) {
				return defaultValue;
			}

			if (json [key] == null) {
				return defaultValue;
			}

			return json [key];
		}
	}
}

